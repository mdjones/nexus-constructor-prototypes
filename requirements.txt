PySide2
xmltodict
gitpython
numpy-stl
pint
git+https://github.com/ess-dmsc/python-nexus-utilities@6f5d2c7f1ca66063227aad2a111a2cc567d573f6#egg=nexusutils
attr

# Formatting/linting
black
pre-commit
