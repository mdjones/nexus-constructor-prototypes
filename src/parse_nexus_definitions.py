import xmltodict
from git import Repo
import os
from typing import List

"""
Demonstrate parsing the nexusformat definitions

Requires:
xmltodict
gitpython
"""


def __clone_or_update_definitions(repo_directory):
    try:
        repo = Repo(repo_directory)
        assert not repo.bare
        origin = repo.remotes.origin
        origin.pull()
    except:
        Repo.clone_from(
            "https://github.com/nexusformat/definitions.git", repo_directory
        )


def __list_base_class_files(repo_directory):
    base_class_dir = repo_directory + "/base_classes"
    for file in os.listdir(base_class_dir):
        if file.endswith(".nxdl.xml"):
            yield os.path.join(base_class_dir, file)


def make_dictionary_of_class_definitions(
    repo_directory="nexus_definitions", black_list: List[str] = None
):
    if black_list is None:
        black_list = []

    __clone_or_update_definitions(repo_directory)
    class_definitions = {}
    for base_class_file in __list_base_class_files(repo_directory):
        with open(base_class_file) as def_file:
            xml_definition = xmltodict.parse(def_file.read())["definition"]
            nx_class_name = xml_definition["@name"]
            if nx_class_name in black_list:
                continue
            class_definitions[nx_class_name] = []
            try:
                fields = xml_definition["field"]
                try:
                    for field in fields:
                        class_definitions[nx_class_name].append(field["@name"])
                except:
                    class_definitions[nx_class_name].append(fields["@name"])
            except KeyError:
                # No key called "field"
                pass
    return class_definitions


if __name__ == "__main__":
    from pprint import pprint

    class_defs = make_dictionary_of_class_definitions(black_list=["NXentry"])
    pprint(class_defs)
