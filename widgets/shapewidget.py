from PySide2.QtWidgets import QWidget, QVBoxLayout, QPushButton, QSizePolicy
from PySide2.QtCore import QSize, QUrl
from PySide2.QtQuickWidgets import QQuickWidget


class ShapeWidget(QWidget):
    def __init__(self, parent=None):
        super(ShapeWidget, self).__init__(parent)

        select_file_button = QPushButton("Select shape file")
        select_file_button.clicked.connect(self.select_new_file)

        self.shape_3d_display = QQuickWidget()
        size_policy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        size_policy.setHorizontalStretch(0)
        size_policy.setVerticalStretch(0)
        size_policy.setHeightForWidth(
            self.shape_3d_display.sizePolicy().hasHeightForWidth()
        )
        self.shape_3d_display.setSizePolicy(size_policy)
        self.shape_3d_display.setMinimumSize(QSize(400, 300))
        self.shape_3d_display.setResizeMode(QQuickWidget.SizeRootObjectToView)
        self.shape_3d_display.setSource(QUrl("file:./qml/view.qml"))

        self.main_layout = QVBoxLayout()
        self.main_layout.addWidget(select_file_button)
        self.main_layout.addWidget(self.shape_3d_display)
        self.setLayout(self.main_layout)

    def select_new_file(self):
        pass

    def new_file_selected(self):
        pass
