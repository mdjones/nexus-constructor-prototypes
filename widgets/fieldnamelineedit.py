from PySide2.QtWidgets import QCompleter, QLineEdit
from PySide2.QtCore import QStringListModel
from PySide2.QtCore import Qt
from typing import List


class FieldNameLineEdit(QLineEdit):
    def __init__(self, possible_field_names: List[str]):
        super().__init__()
        self.setCompleter(QCompleter())
        model = QStringListModel()
        model.setStringList(possible_field_names)
        self.completer().setModel(model)
        self.setPlaceholderText("Enter name of new field")

    def focusInEvent(self, event):
        self.completer().complete()
        super(FieldNameLineEdit, self).focusInEvent(event)

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Down:
            self.completer().complete()
        else:
            super().keyPressEvent(event)
