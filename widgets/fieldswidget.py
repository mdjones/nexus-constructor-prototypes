from PySide2.QtWidgets import QWidget, QVBoxLayout, QListView
from PySide2.QtGui import QStandardItemModel, QStandardItem
from .fieldnamelineedit import FieldNameLineEdit
from typing import List


class FieldsWidget(QWidget):
    def __init__(self, possible_field_names: List[str], parent=None):
        super(FieldsWidget, self).__init__(parent)

        self.field_name_edit = FieldNameLineEdit(possible_field_names)
        self.field_name_edit.editingFinished.connect(self.new_field_name_entered)

        view = QListView(self)
        self.field_list_model = QStandardItemModel()
        view.setModel(self.field_list_model)

        self.main_layout = QVBoxLayout()
        self.main_layout.addWidget(self.field_name_edit)
        self.main_layout.addWidget(view)
        self.setLayout(self.main_layout)

    def new_field_name_entered(self):
        new_field_name = self.field_name_edit.text()
        if new_field_name:
            if not self.field_list_model.findItems(new_field_name):
                self.field_list_model.appendRow(
                    QStandardItem(self.field_name_edit.text())
                )
                self.field_name_edit.setText(None)
