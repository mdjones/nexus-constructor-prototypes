from PySide2.QtWidgets import QWidget, QVBoxLayout, QLineEdit, QLabel, QFormLayout
from .fieldswidget import FieldsWidget
from .shapewidget import ShapeWidget
from typing import List


class EditComponentWidget(QWidget):
    def __init__(self, possible_field_names: List[str], parent=None):
        super(EditComponentWidget, self).__init__(parent)

        self.component_name_edit = QLineEdit()
        self.description_edit = QLineEdit()
        self.description_edit.setPlaceholderText("(Optional)")

        form_layout = QFormLayout()
        form_layout.addRow(QLabel("Name:"), self.component_name_edit)
        form_layout.addRow(QLabel("Description:"), self.description_edit)

        shape_widget = ShapeWidget()
        self.fields = FieldsWidget(possible_field_names)

        self.main_layout = QVBoxLayout()
        self.main_layout.addLayout(form_layout)
        self.main_layout.addWidget(shape_widget)
        self.main_layout.addWidget(self.fields)
        self.setLayout(self.main_layout)
