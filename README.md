# Prototypes for NeXus Constructor

NeXus Constructor: https://github.com/ess-dmsc/nexus-constructor

Install requirements:
```
pip install -r requirements.txt
```

Install pre-commit hook which takes care of formatting (with [black](https://github.com/python/black)):
```
pre-commit install
```
