from src.parse_nexus_definitions import make_dictionary_of_class_definitions
from PySide2.QtWidgets import (
    QMainWindow,
    QApplication,
    QVBoxLayout,
    QComboBox,
    QPushButton,
    QWidget,
)
import sys
from typing import List, Dict
from editcomponentwindow import EditComponentWindow


class PrototypeWidget(QWidget):
    def __init__(self, nexus_defs: Dict[str, List[str]]):
        super().__init__()
        self.nexus_definitions = nexus_defs

        self.nx_class_select = QComboBox()
        self.nx_class_select.addItems(list(self.nexus_definitions.keys()))

        self.button = QPushButton("Show edit dialog")
        self.button.clicked.connect(self.show_edit_dialog)

        main_layout = QVBoxLayout()
        main_layout.addWidget(self.nx_class_select)
        main_layout.addWidget(self.button)

        self.setLayout(main_layout)

    def show_edit_dialog(self):
        dialog = EditComponentWindow(
            self.nx_class_select.currentText(),
            self.nexus_definitions[self.nx_class_select.currentText()],
        )
        dialog.exec_()
        dialog.show()


class Prototype(QMainWindow):
    def __init__(self):
        super().__init__()
        self.statusBar().showMessage("Loading NeXus Definitions")
        # Black list NXentry and legacy classes
        blacklist = [
            "NXentry",
            "NXgeometry",
            "NXtranslation",
            "NXshape",
            "NXorientation",
        ]
        nexus_definitions = make_dictionary_of_class_definitions(black_list=blacklist)
        self.statusBar().showMessage("")

        main_widget = PrototypeWidget(nexus_definitions)
        self.setWindowTitle("Prototype Demo")
        self.setCentralWidget(main_widget)

        self.show()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    ex = Prototype()
    sys.exit(app.exec_())
