from PySide2.QtWidgets import QDialog, QHBoxLayout, QSizePolicy
from PySide2.QtWebEngineWidgets import QWebEngineView
from PySide2.QtCore import QUrl
from widgets.editcomponentwidget import EditComponentWidget
from typing import List


class EditComponentWindow(QDialog):
    def __init__(self, nx_class_name: str, possible_field_names: List[str]):
        super().__init__()
        self.setWindowTitle(nx_class_name)

        self.edit_component = EditComponentWidget(possible_field_names)

        self.documentation_webpage = QWebEngineView()
        self.documentation_webpage.load(
            QUrl(
                f"http://download.nexusformat.org/sphinx/classes/base_classes/{nx_class_name}.html"
            )
        )

        size_policy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        size_policy.setHorizontalStretch(1)

        self.edit_component.setSizePolicy(size_policy)
        self.documentation_webpage.setSizePolicy(size_policy)

        main_layout = QHBoxLayout()
        main_layout.addWidget(self.edit_component)
        main_layout.addWidget(self.documentation_webpage)

        self.setLayout(main_layout)


if __name__ == "__main__":
    import sys
    from PySide2.QtWidgets import QApplication

    app = QApplication(sys.argv)
    edit_component = EditComponentWindow(
        "NXevent_data",
        [
            "event_time_offset",
            "event_time_zero",
            "event_id",
            "event_index",
            "pulse_height",
            "cue_timestamp_zero",
            "cue_index",
        ],
    )

    edit_component.show()
    sys.exit(app.exec_())
